package com.company;

import java.util.EmptyStackException;
import java.util.concurrent.Semaphore;


public  class ConcurrentQueueUsingMutex<T> {
    private Semaphore mutex;
    private Node<T> head;
    private Node<T> tail;

    public ConcurrentQueueUsingMutex() {
        this.mutex = new Semaphore(1, true); // fair -true предоставление мьютекса потокам в том порядке в котором запрошен
    }

    private class Node<T> {
        private Node<T> next;
        private T obj;

        public Node(T obj) {
            this.obj = obj;
        }
    }

    public void push(T obj) throws InterruptedException {
        mutex.acquire();
        if (head == null) {
            head = new Node<T>(obj);
            tail = head;
        } else {
            Node<T> temp = new Node<>(obj);
            tail.next = temp;
            tail = temp;
            tail.next = null;
        }
        System.out.println(Thread.currentThread().getName() + ":" + "Добавлено:" + obj.toString());
        mutex.release();

    }

    public T pop() throws InterruptedException, EmptyStackException {
        mutex.acquire();
        if( head == null){
            mutex.release();
            throw new EmptyStackException();
        }
        Node<T> temp = null;
        if (head.next == null) {
            temp = head;
            head = null;
        } else {
            temp = head;
            head = head.next;
        }
        System.out.println(Thread.currentThread().getName() + ":" + "Удалено:" + temp.obj.toString());
        mutex.release();
        return temp.obj;
    }
}

