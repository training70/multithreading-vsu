package com.company;


import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

public class FourthLaboratory {
    private static ConcurrentQueueUsingMutex queue;

    public static void setQueue(ConcurrentQueueUsingMutex queue) {
        FourthLaboratory.queue = queue;
    }

    static class Producer implements Runnable {
        private AtomicInteger count = new AtomicInteger(0);

        Producer(int i) {
            this.count.set(i);
        }

        public void run() {
            if (count.get() > 0) {
                try {
                    queue.push(count.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            count.addAndGet(-1);
        }
    }

    static class Consumer<T> implements Callable {
        private AtomicInteger count = new AtomicInteger(0);

        Consumer(int i) {
            this.count.set(i);
        }

        @Override
        public List<T> call() {
            List<T> temp = new ArrayList<>();
            if (count.get() > 0) {
                try {
                    temp.add((T) queue.pop());
                } catch (EmptyStackException e) {
                    System.out.println("Empty Queue, Try again");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            count.addAndGet(-1);
            return temp;
        }
    }
}
