package com.company;


public class SecondLaboratory {
    static double h;
    private static Double[] integralOnSegments;

    private static class MyThread extends Thread {
        double a;
        double b;
        int n;
        int i;
        private MyThread(double a, double b, int n, int i) {
            this.a = a;
            this.b = b;
            this.n = n;
            this.i = i;

        }

        @Override
        public void run() {
            double tempAmount = 0;
            double h = (b - a) / n;
            for (double j = a + h ; j < b; j += h) {
                tempAmount += function(j);

            }
            integralOnSegments[i] = h * ((function(a) + function(b)) / 2 + tempAmount);
            System.out.println(Thread.currentThread().getName() + ":" + integralOnSegments[i]);
        }
    }

    public static double function(double n) {
        return 1 / Math.log(n);
    }

    public static Double calculate(double a, double b, int n) throws InterruptedException {
        int beans = Runtime.getRuntime().availableProcessors();
        h = (b - a) / beans;
        integralOnSegments = new Double[beans];
        MyThread[] treads = new MyThread[beans];
        double tempA = a;
        for (int i = 0; i < beans; i++) {
            if (i < beans - 1) {
                treads[i] = new MyThread(tempA, tempA += h, n, i);
            } else {
                treads[i] = new MyThread(tempA, b, n, i);
            }

                treads[i].start();
        }
        for (int i = 0; i < beans; i++) {
            treads[i].join();
        }

        double integral = 0;
        for (int i = 0; i < integralOnSegments.length; i++) {
            integral += integralOnSegments[i];
        }
        return integral;
    }

}


