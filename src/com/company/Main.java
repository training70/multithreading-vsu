package com.company;


import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        int n = scanner.nextInt();
        System.out.println("Интеграл 2-ая лабораторная:" + SecondLaboratory.calculate(a,b,n));
        System.out.println("Интеграл 3-ая лабораторная:" + ThirdLaboratory.calculate(a,b,n));
        /**
        FourthLaboratory.setQueue(new ConcurrentQueueUsingMutex());
        ExecutorService executorService = Executors.newFixedThreadPool(7);
        List<Future> futures = new ArrayList<>();
        futures.add(executorService.submit(new FourthLaboratory.Consumer(5)));
        futures.add(executorService.submit(new FourthLaboratory.Producer(5)));
        for (Future future : futures) {
            try {
                System.out.println(future.get());
            } catch (ExecutionException e) {
               e.printStackTrace();
            }
        }
        executorService.shutdown();
       */
   }
}

