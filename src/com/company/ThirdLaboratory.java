package com.company;

import com.google.common.util.concurrent.AtomicDouble;




public class ThirdLaboratory {
    static double h;
    private static AtomicDouble amount = new AtomicDouble(0.d);

    private static class MyThread extends Thread {
        double a;
        double b;
        int n;
        private MyThread(double a, double b, int n) {
            this.a = a;
            this.b = b;
            this.n = n;
        }

        @Override
        public void run() {
            double tempAmount = 0;
            double h = (b - a) / n;
            for (double j = a + h ; j < b; j += h) {
                tempAmount += function(j);

            }
            amount.addAndGet(h * ((function(a) + function(b)) / 2 + tempAmount));
            System.out.println(Thread.currentThread().getName() + ":" + amount.get());
        }
    }

    public static double function(double n) {
        return 1 / Math.log(n);
    }

    public static Double calculate(double a, double b, int n) throws InterruptedException {
        int beans = Runtime.getRuntime().availableProcessors();
        h = (b - a) / beans;
        MyThread[] treads = new MyThread[beans];
        double tempA = a;
        for (int i = 0; i < beans; i++) {
            if (i < beans - 1) {
                treads[i] = new MyThread(tempA, tempA += h, n);
            } else {
                treads[i] = new MyThread(tempA, b, n);
            }

            treads[i].start();
        }
        for (int i = 0; i < beans; i++) {
            treads[i].join();
        }

        return amount.get();
    }
}
